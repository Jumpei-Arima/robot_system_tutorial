#include "ros/ros.h"
#include "std_msgs/String.h"

class Listener{
    public:
        Listener();
        void ChatterCallback(const std_msgs::StringConstPtr& msg);
    private:
        ros::NodeHandle nh;
        //subscriber
        ros::Subscriber chatter_sub;
};

Listener::Listener()
{
    //subscriber
    chatter_sub = nh.subscribe("chatter", 10, &Listener::ChatterCallback, this);
}

void Listener::ChatterCallback(const std_msgs::StringConstPtr& msg)
{
    ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "listener");
    Listener listener;
    ros::spin();
    return 0;
}
