#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

class Talker{
    public:
        Talker();
        void process(double);
    private:
        // ノードハンドル
        ros::NodeHandle nh;
        // publisher
        ros::Publisher chatter_pub;
};

Talker::Talker()
{
    /* publisherの初期化
        トピック名: chatter
        トピックの型: std_msgs::String
        キューサイズ: 10
    */
    chatter_pub = nh.advertise<std_msgs::String>("chatter", 10);
}

void Talker::process(const double loop_hz)
{
    //　ループの周期[Hz]の設定
    ros::Rate loop_rate(loop_hz);
    int count = 0;
    // メインループ
    while (ros::ok())
    {
        std_msgs::String msg;

        std::stringstream ss;
        ss << "hello world " << count;
        msg.data = ss.str();

        ROS_INFO("%s", msg.data.c_str());

        chatter_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
        ++count;
    }
}

int main(int argc, char **argv)
{
    // rosノードの初期化
    ros::init(argc, argv, "talker");
    double loop_hz = 10.0;
    Talker talker;
    talker.process(loop_hz);
    return 0;
}
