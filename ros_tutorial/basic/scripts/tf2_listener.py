#!/usr/bin/env python  
import rospy

import math
import tf2_ros
import geometry_msgs.msg
import turtlesim.srv

if __name__ == '__main__':
    rospy.init_node('tf2_listener')

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)
    world_frame = "world"
    robot_frame = "base_link"
    sensor_frame = "laser"

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            trans = tfBuffer.lookup_transform(robot_frame, sensor_frame, rospy.Time())
            trans2 = tfBuffer.lookup_transform(world_frame, robot_frame, rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            print(e)
            rate.sleep()
            continue

        print("tf from %s to %s :" % (robot_frame, sensor_frame))
        print(trans)
        print("tf from %s to %s :" % (world_frame, robot_frame))
        print(trans)
        print(trans2)
        rate.sleep()