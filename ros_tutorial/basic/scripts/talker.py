#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    r = rospy.Rate(10)
    count = 0
    while not rospy.is_shutdown():
        str = "hello world %s" % count
        rospy.loginfo(str)
        pub.publish(str)
        count += 1
        r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
