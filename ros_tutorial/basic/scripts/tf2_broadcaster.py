#!/usr/bin/env python

import random
import math

import rospy

import tf_conversions
import tf2_ros
import geometry_msgs.msg

def state_transition(pose, action, DT):
    pre_theta = pose[2]
    if abs(action[1])<1e-10:
        pose[0] += action[0]*math.cos(pre_theta)*DT
        pose[1] += action[0]*math.sin(pre_theta)*DT
        pose[2] += action[1]*DT
    else:
        pose[0] += action[0]/action[1]*(math.sin(pre_theta+action[1]*DT)-math.sin(pre_theta))
        pose[1] += action[0]/action[1]*(-math.cos(pre_theta+action[1]*DT)+math.cos(pre_theta))
        pose[2] += action[1]*DT
    pose[2] = math.atan2(math.sin(pose[2]), math.cos(pose[2]))
    return pose

if __name__ == '__main__':
    rospy.init_node('tf2_broadcaster')
    world_frame = "world"
    robot_frame = "base_link"

    br = tf2_ros.TransformBroadcaster()
    pose = [0.0, 0.0, 0.0]

    HZ = 10.0
    rate = rospy.Rate(HZ)
    while not rospy.is_shutdown():
        linear_vel = random.random()
        angular_vel = random.random()*2.0-1.0
        pose = state_transition(pose, [linear_vel, angular_vel], 1.0/HZ)
        t = geometry_msgs.msg.TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = world_frame
        t.child_frame_id = robot_frame
        t.transform.translation.x = pose[0]
        t.transform.translation.y = pose[1] 
        t.transform.translation.z = 0.0
        q = tf_conversions.transformations.quaternion_from_euler(0, 0, pose[2])
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]

        br.sendTransform(t)
        print(pose)
        rate.sleep()