#!/usr/bin/env python
import rospy

import sys
import tf

import tf2_ros
import geometry_msgs.msg

if __name__ == '__main__':
    rospy.init_node('static_tf2_broadcaster')
    robot_frame = "base_link"
    sensor_frame = "laser"

    broadcaster = tf2_ros.StaticTransformBroadcaster()
    static_transformStamped = geometry_msgs.msg.TransformStamped()

    static_transformStamped.header.stamp = rospy.Time.now()
    static_transformStamped.header.frame_id = robot_frame
    static_transformStamped.child_frame_id = sensor_frame

    static_transformStamped.transform.translation.x = 0
    static_transformStamped.transform.translation.y = 0
    static_transformStamped.transform.translation.z = 0.3

    quat = tf.transformations.quaternion_from_euler(0.0, 0.0, 0.0)
    static_transformStamped.transform.rotation.x = quat[0]
    static_transformStamped.transform.rotation.y = quat[1]
    static_transformStamped.transform.rotation.z = quat[2]
    static_transformStamped.transform.rotation.w = quat[3]
    broadcaster.sendTransform(static_transformStamped)
    print("Spinning until killed publishing %s to %s" %(robot_frame, sensor_frame))
    rospy.spin()