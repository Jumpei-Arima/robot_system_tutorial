#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>

class RobotControl{
public:
	RobotControl();

	void ScanCallback(const sensor_msgs::LaserScanConstPtr& msg);
	void process();

private:
	ros::NodeHandle nh;
	ros::NodeHandle local_nh;

	// subscriber
	ros::Subscriber scan_sub;

	//publisher
	ros::Publisher vel_pub;

	//tf
	tf::TransformListener listener;

	ros::Time current_time;
	ros::Time last_time;
	sensor_msgs::LaserScan scan;
	bool scan_received;

	int HZ;
	std::string ODOM_FRAME;
	std::string ROBOT_FRAME;
};

RobotControl::RobotControl()
	:local_nh("~")
{
	// subscriber
	scan_sub = nh.subscribe("/scan", 100, &RobotControl::ScanCallback, this);
	
	//publisher
	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 100);

	scan_received = false;

	local_nh.param("HZ", HZ, {10});
	local_nh.param("ODOM_FRAME", ODOM_FRAME, {"odom"});
	local_nh.param("ROBOT_FRAME", ROBOT_FRAME, {"base_footprint"});
}

void RobotControl::ScanCallback(const sensor_msgs::LaserScanConstPtr& msg)
{
	scan = *msg;
	scan_received = true;
}

void RobotControl::process()
{
	current_time = ros::Time::now();
	last_time = current_time;
	ros::Rate loop_rate(HZ);
	while(ros::ok()){
		current_time = ros::Time::now();
		// tf listener
		tf::StampedTransform transform;
		geometry_msgs::PoseStamped pose;
		try{
			listener.lookupTransform(ODOM_FRAME, ROBOT_FRAME, ros::Time(0), transform);
			tf::poseStampedTFToMsg(tf::Stamped<tf::Transform>(transform, transform.stamp_, transform.frame_id_), pose);
		}catch(tf::TransformException ex){
			std::cout << ex.what() << std::endl;;
		}	

		geometry_msgs::Twist vel;
		if(scan_received){
            int min_index = 0;
            double min_range = 100000;
            for(int i=scan.ranges.size()/4; i<3*scan.ranges.size()/4;i++){
                if(scan.ranges[i]<min_range){
                    min_range = scan.ranges[i];
                    min_index = i;
                }
            }
			std::cout << "scan_size: " << scan.ranges.size() << std::endl;
			std::cout << "min_index: " << min_index << std::endl;
			std::cout << "min_range: " << min_range << std::endl;
			if(min_range > 0.3){
				vel.linear.x = 0.1;
				vel.angular.z = 0.0;
				std::cout << "go straight" << std::endl;
			}else if(min_index < scan.ranges.size()/2){
				vel.linear.x = 0.0;
				vel.angular.z = 0.3;
				std::cout << "turn left" << std::endl;
			}else{
				vel.linear.x = 0.0;
				vel.angular.z = -0.3;
				std::cout << "turn right" << std::endl;
			}
		}else{
			vel.linear.x = 0.0;
			vel.angular.z = 0.0;
		}
		vel_pub.publish(vel);
		// ROS_INFO("dt:%f", current_time.toSec()-last_time.toSec());
		// std::cout << "pose: " << pose << std::endl;
		last_time = current_time;
		ros::spinOnce();
		loop_rate.sleep();
	}
}
int main(int argc, char** argv)
{
	ros::init(argc, argv, "robot_sim");
	RobotControl robot;
	robot.process();
  
	return 0;
}