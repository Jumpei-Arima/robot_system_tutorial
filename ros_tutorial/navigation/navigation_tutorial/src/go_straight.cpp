#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>

class RobotControl{
public:
	RobotControl();

	void process();

private:
	ros::NodeHandle nh;
	ros::NodeHandle local_nh;

	//publisher
	ros::Publisher vel_pub;

	//tf
	tf::TransformListener listener;

	ros::Time current_time;
	ros::Time last_time;

	int HZ;
	std::string ODOM_FRAME;
	std::string ROBOT_FRAME;
};

RobotControl::RobotControl()
	:local_nh("~")
{
	//publisher
	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 100);

	local_nh.param("HZ", HZ, {10});
	local_nh.param("ODOM_FRAME", ODOM_FRAME, {"odom"});
	local_nh.param("ROBOT_FRAME", ROBOT_FRAME, {"base_link"});

}

void RobotControl::process()
{
	current_time = ros::Time::now();
	last_time = current_time;
	ros::Rate loop_rate(HZ);
	while(ros::ok()){
		current_time = ros::Time::now();
		// tf listener
		tf::StampedTransform transform;
		geometry_msgs::PoseStamped pose;
		try{
			listener.lookupTransform(ODOM_FRAME, ROBOT_FRAME, ros::Time(0), transform);
			tf::poseStampedTFToMsg(tf::Stamped<tf::Transform>(transform, transform.stamp_, transform.frame_id_), pose);
		}catch(tf::TransformException ex){
			std::cout << ex.what() << std::endl;;
		}	

		geometry_msgs::Twist vel;
		if(pose.pose.position.x<3.0){
			vel.linear.x = 0.5;
		}else{
			vel.linear.x = 0.0;
		}
		vel_pub.publish(vel);

		ROS_INFO("dt:%f", last_time.toSec()-current_time.toSec());
		std::cout << "pose: " << pose << std::endl;
		last_time = current_time;
		ros::spinOnce();
		loop_rate.sleep();
	}
}
int main(int argc, char** argv)
{
	ros::init(argc, argv, "robot_sim");
	RobotControl robot;
	robot.process();
  
	return 0;
}
