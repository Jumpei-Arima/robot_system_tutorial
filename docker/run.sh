#!/bin/sh

IMAGE_NAME=arijun/ros_robot_system_tutorial:melodic
CONTAINER_NAME=robot_system_tutorial
echo ${IMAGE_NAME}
echo ${CONTAINER_NAME}

CONTAINER_ID=`docker ps -aq -f name=${CONTAINER_NAME}`
if [ ! -z "${CONTAINER_ID}" ]; then
    STATUS=`docker inspect --format='{{.State.Status}}' ${CONTAINER_NAME}`
    echo ${STATUS}
    if [ "${STATUS}" = "exited" ]; then
        docker start ${CONTAINER_NAME}
    fi
    docker exec -it ${CONTAINER_NAME} bash
else
    docker run -it \
        --privileged \
        -p 5900:5900 \
        -v $PWD/../ros_tutorial/:/root/ros_catkin_ws/src/ \
        -e DISPLAY=:0 \
        --name ${CONTAINER_NAME} \
        ${IMAGE_NAME} \
        bash
fi
