はじめに
===============

このドキュメントについて
-------------------------
- ロボットをシミュレータ及び実機で動かすのに最低限必要になるrobot systemのチュートリアル
- ROSとpybulletを中心に実装例集をベースにしたもの

想定対象
---------
- ロボットを実機もしくはシミュレータで動かしたい学生
- python（できればc++）の基本的なプログラミングができる

何ができるようになる？
----------------------
- ROSを使ったロボットシステムの構築
- OSSに上がっている手法をシミュレータ・実機で再現するための基礎的な技術の獲得
- ....

環境構築
-------------------
- dependencies
   - docker
   - git

- インストール

.. code-block:: bash

    $ git clone https://github.com/Jumpei-Arima/robot_system_tutorial.git

- Dockerfileのビルド

.. code-block:: bash

    $ cd robot_system_tutorial/docker
    $ ./build.sh

- Docker Containerの起動

.. code-block:: bash

    $ cd robot_system_tutorial/docker
    $ ./run.sh

- catkin wsのビルド

.. code-block:: bash
    
    $ cd ros_catkin_ws
    $ catkin build

- vncの起動

.. code-block:: bash

    $ ./run_vnc.sh

- vnc viewerで接続

   - google chrome appのvnc viewer
   - macならfinder→移動→サーバーへ接続(⌘+K)

   デフォルトの設定では

   - portは5900ポート `vnc://localhost:5900`
   - パスワードは `pass`