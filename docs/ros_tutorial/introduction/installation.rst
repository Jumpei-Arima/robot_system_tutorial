==================
ROSのインストール
==================

--------
概要
--------
ROSはUbuntuにインストールする方法について説明します。
(本documentのdocker imageを使えばUbuntuだけでなく, mac, windows上でも動作可能です。)

--------------------
ROSのdistribution
--------------------
ROSのdistributionはubuntuのバージョンとリンクして上がっています。
http://wiki.ros.org/Distributions

本ドキュメントのdocker imageはUbuntu18.04にROSのmelodicがインストールされています。

Ubuntuのhostにインストールする場合は、以下のリンク通りにすることでインストールが可能です。
http://wiki.ros.org/melodic/Installation/Ubuntu


catkinワークスペースの作成
-----------------------------

ROSの開発は、作業用ワークスペースとしてcatkinワークスペースを作成します。
catkinは、プログラムのビルドや依存関係の解決を簡単に行うためのROSのビルドシステムです。
ホーム直下にcatkinワークスペースを作成するときの例です。

.. code-block:: bash

    $ mkdir -p ~/catkin_ws/src
    $ cd ~/catkin_ws/src
    $ catkin_init_workspace

`~/catkin_ws/src`の下に`CMakeLists.txt`が作成されます。

catkinを使ったbuildの方法です。ワークスペース直下で`catkin build`とするだけです。

.. code-block:: bash

    $ cd ~/catkin_ws
    $ catkin build

catkin buildのoptionで`catkin build -j 24`などとすると、ビルドの並列処理数が変わります。(rasberry piなどではこれを低くしないと重すぎてフリーズしてしまうことがあります。)

設定しておくと便利
---------------------

以下を`~/.bashrc` (or '~/.zshrc')に書いておくと便利なことがあります。

.. code-block:: bash

    ## ROS ##
    source ${HOME}/carkin_ws/devel/setup.bash
    export ROS_PACKAGE_PATH=/${HOME}/catkin_ws/:$ROS_PACKAGE_PATH
    export ROS_WORKSPACE=${HOME}/ros_catkin_ws

`roscd`を実行すると通常の場合`/opt/ros/{distro}`に移動しますが、これを設定すると`~/catkin_ws/`に移動してくれます。

<注意>

hostにROSを入れるとcv2を実行するときにerrorが出る

原因：ROSをインストール時にcv2が依存ファイルとしてインストールされるため

解決策：

- `~/.bashrc`のROSに関連するもの（`source /opt/ros/melodic/setup.bash`など）をコメントアウト　
- python scriptの最初にsys.path.removeで(rosのcv2へのpathを削除する)
- hostにROSを入れない(dockerを使う)