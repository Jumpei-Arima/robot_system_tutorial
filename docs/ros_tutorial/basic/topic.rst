================
ROSのtopic通信
================

ロボットを動作させるには複数の機能を並列的に実行することが多いです。
それらのプログラムがデータをやり取りする通信ライブラリの仕組みをROSは提供してくれます。

ROSはプログラムを小さなプロセスに分割し、それらのプロセス間通信を統一的に扱うことができます。
これにより、プログラムの再利用化、スケーラビリティの確保、システム構築の構築と検証の容易化などのメリットがあります。

ROSでは、一つのプログラム(プロセス)単位をノード(Node)と呼びます。
このノード間のデータ通信には、メッセージという形でデータを送受信します。
各ノードはメッセージという形でデータを他のノードに送り、データを受けたノードは処理結果を次のノードに渡すことで、全体のシステムが動作します。
したがって、ノードの動作は基本的にイベントドリブンな形式で記述されます。

例： カメラ画像を取得して認識を行うプログラムの場合
    カメラノード→（生画像データ）→前処理ノード→（前処理済みの画像データ）→認識ノード→（認識結果）

多数のセンサやアクチュエータが搭載されているロボットの場合、それぞれデバイスドライバが必要になりますが、
多くのセンサやアクチュエータのROSのデバイスドライバは、世界中のROSコミュニティのメンバから公開されており、購入したデバイスを購入後すぐに組み込んで利用することができます、
これはROSの大きな特徴の一つです。

ROSの標準的なデータ通信の経路をtopic（トピック）と呼び、Pub／Subモデルという形式で通信を行います。

- publish: ノードがトピックにデータを送って配信すること
- subscribe: ノードがトピックからデータを受信すること

----------------
message
----------------
topicで送受信されるデータはメッセージと呼ばれ、メッセージの型は使用言語に依存しないデータ形式となっています。

標準でインストールされているメッセージは`＄rosmsg list`で一覧を見ることができます。
messegeファイルの中身はコマンドからでもrosのドキュメントからでも確認することができます。

例：　geometry_msgs/Pose型の場合

- `rosmsg show geometry_msgs/Pose`
- http://docs.ros.org/melodic/api/geometry_msgs/html/msg/Pose.html

自分でmessageを生成することも可能です。
vhttp://wiki.ros.org/ja/ROS/Tutorials/CreatingMsgAndSrv

--------------
ROS master
--------------

ROSはROS masterというROSのネットワーク通信を管理するものがあります。
ROS masterはrosのノードや後述するサービスなどは名前登録を行い、
他のノードなどから見えるようにする役割があります。
以下のコマンドでROS masterが起動します。

.. code-block:: bash

    $ roscore

------------------
Publisher
------------------

sample code: ~/ros_catkin_ws/src/basic/scripts/talker.py

chatterというトピック名のメッセージをpublishするノードです。
トピックの型はstd_msgsのString型
10Hzごとにトピックがpublishされるノードになります。

実行コマンド

.. code-block:: bash

    $ rosrun basic talker.py

-------------------
Subscriber
-------------------

sample code: ~/ros_catkin_ws/src/basic/scripts/listener.py

std_msgsのString型のchatterというトピック名のメッセージを受信し、受信内容を標準出力するノードです。
chatter topicが受信されるとcallback関数が呼ばれます。

実行コマンド

.. code-block:: bash

    $ rosrun basic listener.py