==================
ROSのservice通信
==================

ROSのtopic通信は、topicの通信相手のことは考えない非同期通信です。
一方ROSでは同期通信を行う場合は、serviceを使います。

----------
service
----------

ここでは、自作のサービスの型を作成します。

sample code: ~/ros_catkin_ws/src/basic/srv/AddTwoInts.srv

serviceの型はrequestとresponseで構成され、---で区切って表記します。
上記の例の場合、requestがint64型のaとbで、
responseがint64型のsumとなります。

自作のserviceを使用するには、CMakelists.txtに記述しbuildする必要があります。
具体的には、~/ros_catkin_ws/src/basic/CMakeLists.txt と以下のサイトを参照してください。
http://wiki.ros.org/ja/ROS/Tutorials/CreatingMsgAndSrv


-------------------
Server
-------------------

sample code: ~/ros_catkin_ws/src/basic/scripts/add_two_ints_server.py

サービスを提供するノード。
AddTwoInts.srv型のrequestとして与えられる二つの整数の和をresponseとして送り返します。

実行コマンド

.. code-block:: bash

    $ rosrun basic add_two_ints_server.py

------------------
Client
------------------

sample code: ~/ros_catkin_ws/src/basic/scripts/add_two_ints_client.py

サービスを呼び出すノード。
整数二つで構成されるAddTwoInts.srv型のrequestを送り、responseを受け取ります。
整数は実行時引数として与えます。

実行コマンド

.. code-block:: bash

    $ rosrun basic add_two_ints_client.py 3 4