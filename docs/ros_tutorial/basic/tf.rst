==================
ROSのtf
==================

ロボットのシステムでは、多数の座標系を扱います。
（世界座標系、地図座標系、カメラ座標系、ロボット座標系など）

これらの座標系を統一的に扱えるようにする機能を持つのが、ROSのtfです。

tfの役割は、非同期に配信される二つの座標系(以下フレームという)の位置関係を取得する機能と、
フレーム間のtree構造を構築する機能の二つに大きく分けることができます。

前者の機能により、分散システムであるROSとの高い親和性を発揮することができます。
また後者の機能により、複雑な座標変換の計算を利用者に意識させないユーザビリティを提供することができます。

上で示した二つの要素の機能はそれぞれ以下の二つで実装できます。
- broadcaster: フレーム間の相対位置を配信する
- listener: フレーム間の相対位置を取得する


------------------
broadcaster
------------------

sample code: ~/ros_catkin_ws/src/basic/scripts/static_tf2_broadcaster.py

robotのベースの座標系(robot_frame)とセンサの座標系(sensor_frame)の相対位置をbroadcastするノードです。
このように固定の座標系をtfでbroadcastする場合は、StaticTransformBroadcaster()を用います。
また、tfに限らずrosにおける姿勢の表現には、四元数(quaternion)を用います。
なのでここではtfの関数（quaternion_from_euler）でオイラー角からの変換を行っています。

実行コマンド

.. code-block:: bash
    
    $ rosrun basic static_tf2_broadcaster.py

sample code: ~/ros_catkin_ws/src/basic/scripts/tf2_broadcaster.py

world座標系とrobot座標系の相対位置関係をbroadcastするノードです。
このように相対位置関係が時事刻々変化する場合は、TransformBroadcaster()を用います。
それ以外の書き方は上記のstaticな場合と同様です。
実装では、ランダムに行動する差動二輪型のロボットを想定しています。

実行コマンド

.. code-block:: bash
    
    $ rosrun basic tf2_broadcaster.py

-------------------
listener
-------------------

sample code: ~/ros_catkin_ws/src/basic/scripts/tf2_listener.py

ある二つのフレーム間の相対位置関係を取得するノードです。
ここでは直接tree構造がつながっているフレーム間以外の関係も取得することが可能です。
tfの内部で座標変換をして、相対位置関係を算出してくれます。

実行コマンド

.. code-block:: bash

    $ rosrun basic tf2_listener.py