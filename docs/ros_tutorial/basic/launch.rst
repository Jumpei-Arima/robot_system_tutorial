==================
ROSのlaunch
==================

topic、service、tfなどの説明をしてきましたが、
ノードを起動する時はrosrunコマンドを使用していました。


これだと、複数のノードを起動する度にterminalを起動する必要がありましたが、
launchファイルを記述することで、ROSには複数のノードを起動することができます。

launchファイルはxml形式で記述されます。

ノードの起動設定、パラメータの設定、名前空間やリマップの設定、自動再起動の設定などができます。

基本的なlaunchの実行方法は以下になります。

.. code-block:: bash

    $ roslaunch [package_name] [launchfile_name].launchfile_name

またlaunchファイル内に記したargを実行時引数として受け取ることも可能です。

また、ROSの可視化ツールである、rqtやrvizなどもここから起動することが可能です。

以下に、topic通信とtfのチュートリアルで行ったものをlaunchで起動する例を示します。


-----------------------
topic 通信のtutorial
-----------------------

sample code: ~/ros_catkin_src/src/basic/launch/communication_py.launch

talkerとlistenerを起動しています。
listenerの標準出力のみterminalに表示されるようになっています。

また、起動しているノードやトピックの繋がりを可視化するrqt_graphも起動しています。

.. code-block:: bash

    $ roslaunch basic communication_py.launch


-----------------------
tfのtutorial
-----------------------

sample code: ~/ros_catkin_src/src/basic/launch/tf_tutorial.launch

robot内のstaticな関係を出力するstatic_tf_broadcasterと、
時事刻々変化する関係を出力するtf2_broadcaster、
そしてそれらの関係を取得するtf2_listenerの3つを起動しています。
topicと同様listenerのみ標準出力をonにしています。

今回は、tfの関係を可視化するためにrvizと、tree構造を可視化してくれるrqt_tf_treeも同時に起動しています。

.. code-block:: bash

    $ roslaunch basic tf_tutorial.launch